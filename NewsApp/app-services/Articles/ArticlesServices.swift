//
//  ArticlesServices.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import Foundation

protocol ArticleServicesProtocol : class {
    func didGetData(response : ArticlesResponseModel)
    func didFailToGetData()
}

class ArticleServices{
    
    var delegate : ArticleServicesProtocol?
    
    func getArticle(byCategory category: String) {
        let url: String = String.init(format: "https://newsapi.org/v2/sources?apiKey=%@&category=%@", AppConfig.newsApiKey, category)
        var urlRequest = URLRequest(url: URL(string: url)!)
        urlRequest.httpMethod = "GET"
        let boundary = "abcdefghij"
        let contentType = "application/json; boundary=\(boundary)"
        urlRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
        let dataTask = defaultSession.dataTask(with: urlRequest, completionHandler:  {(data, response, error) -> Void in
            if error == nil {
                if let httpResponse = response as? HTTPURLResponse {
                    if let data = data {
                        if httpResponse.statusCode == 200 {
                            if let articleResponse = try? JSONDecoder().decode(ArticlesResponseModel.self, from: data){
                                    self.delegate?.didGetData(response: articleResponse)
                                }else {
                                    self.delegate?.didFailToGetData()
                                }
                            }else {
                                self.delegate?.didFailToGetData()
                            }
                        }else {
                        self.delegate?.didFailToGetData()
                        }
                    }else {
                    self.delegate?.didFailToGetData()
                }
            } else {
                self.delegate?.didFailToGetData()
            }
        })
        
        dataTask.resume()
    }

}
