//
//  ArticleSearchServices.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import Foundation


import Foundation

protocol ArticleSearchResultsServicesProtocol : class {
    func didGetData(response : ArticlesSearchResultResponseModel)
    func didFailToGetData()
}

class ArticleSearchResultsServices{
    
    var delegate : ArticleSearchResultsServicesProtocol?
    
    func getArticle(byKeyword keyword: String) {
        let url: String = String.init(format: "http://newsapi.org/v2/everything?q=%@&apiKey=473e7c47f1764073b93cad04b69827fc",keyword)
        var urlRequest = URLRequest(url: URL(string: url)!)
        urlRequest.httpMethod = "GET"
        let boundary = "abcdefghij"
        let contentType = "application/json; boundary=\(boundary)"
        urlRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
        let dataTask = defaultSession.dataTask(with: urlRequest, completionHandler:  {(data, response, error) -> Void in
            if error == nil {
                if let httpResponse = response as? HTTPURLResponse {
                    if let data = data {
                        if httpResponse.statusCode == 200 {
                                if let articleResponse = try? JSONDecoder().decode(ArticlesSearchResultResponseModel.self, from: data)
                                {
                                    self.delegate?.didGetData(response: articleResponse)
                                }else {
                                    self.delegate?.didFailToGetData()
                                }
                            }else {
                                self.delegate?.didFailToGetData()
                            }
                        }else {
                        self.delegate?.didFailToGetData()
                        }
                    }else {
                    self.delegate?.didFailToGetData()
                }
            } else {
                self.delegate?.didFailToGetData()
            }
        })
        
        dataTask.resume()
    }

}
