//
//  BaseSearchBar.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import UIKit

class BaseSearchBar: UISearchBar {
    
   override func draw(_ rect: CGRect) {
        self.searchTextField.textColor = .white
   }
    
}
