//
//  ArticleListTVCell.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import UIKit

class ArticleListTVCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txDescription: UITextView!
    @IBOutlet weak var lblCountry: UILabel!
    
    
    
    static let identifier = "ArticleListTVCellIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(source: ArticleSource){
        self.lblTitle.text = source.name
        self.lblCountry.text = source.country
        self.txDescription.text = source.sourceDescription
    }
    
    func configCell(element: ArticleElement){
        self.lblTitle.text = element.title
        self.lblTitle.font.withSize(12)
        self.lblCountry.text = element.author
        self.txDescription.text = element.articleDescription
    }
    
}
