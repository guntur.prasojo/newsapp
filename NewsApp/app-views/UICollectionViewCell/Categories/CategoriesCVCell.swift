//
//  CategoriesCollCell.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import UIKit

class CategoriesCollCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var layerView: UIView!
    
    
    static var identifier = "CategoriesCollCellIdentifier"
    
    let cornerRadius : CGFloat = 10
    
    override func awakeFromNib() {
        imgBg.layer.cornerRadius = self.cornerRadius
        imgBg.clipsToBounds = true
        layerView.layer.cornerRadius = self.cornerRadius
        layerView.clipsToBounds = true
        super.awakeFromNib()
    }

}
