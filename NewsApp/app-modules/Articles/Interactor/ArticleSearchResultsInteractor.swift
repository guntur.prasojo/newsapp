//
//  ArticleSearchResultsInteractor.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import Foundation

protocol ArticleSearchResultProtocol :class {
    func didGetData()
    func didFailGetData()
}

class ArticleSearchResultsInteractor{
    var delegate : ArticleSearchResultProtocol?
    var services = ArticleSearchResultsServices()
    var sources : [ArticleElement] = []
    
    var isAbleToLoad : Bool = true
    
    func didLoad(keyword : String){
        services.delegate = self
        if  isAbleToLoad {
            isAbleToLoad = false
            services.getArticle(byKeyword: keyword)
        }
    }
}

extension ArticleSearchResultsInteractor : ArticleSearchResultsServicesProtocol {
  
    
    func didGetData(response: ArticlesSearchResultResponseModel) {
        sources += response.articles
        isAbleToLoad = true
        delegate?.didGetData()
    }
    
    func didFailToGetData() {
        
    }
}
