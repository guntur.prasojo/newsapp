//
//  ArticleListInteractor.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import Foundation

protocol ArticleListProtocol : class {
    func didGetData()
    func isRetrievingData(isRetrieving : Bool)
}

struct ArticleListInteractorPayload{
    var category : String
}

class ArticleListInteractor {
    var delegate : ArticleListProtocol?
    var services = ArticleServices()
    var articleSources : [ArticleSource] = []
    
    var isAbleToLoad : Bool = true
    
    func didLoad(payload : ArticleListInteractorPayload){
        services.delegate = self
        if  isAbleToLoad {
            isAbleToLoad = false
            delegate?.isRetrievingData(isRetrieving: true)
            services.getArticle(byCategory: payload.category)
        }
    }
    
}

extension ArticleListInteractor : ArticleServicesProtocol{
    func didGetData(response: ArticlesResponseModel) {
        self.articleSources += response.sources
        isAbleToLoad = true
        delegate?.didGetData()
        delegate?.isRetrievingData(isRetrieving: false)
    }
    
    func didFailToGetData() {
        print("fail to get data")
    }
    
}
