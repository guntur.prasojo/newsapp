//
//  ArticleListVC.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import UIKit

class ArticleListVC: UIViewController {

    @IBOutlet weak var imgCategoriesTitle: UIImageView!
    @IBOutlet weak var lblCategoriesTitle: UILabel!
    @IBOutlet weak var tblArticlesList: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    
    let searchBar = BaseSearchBar()
    
    var category = "default"
    
    var interactor = ArticleListInteractor()
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Articles", bundle:nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        interactor.delegate = self
        self.showLoading()
        interactor.didLoad(payload: ArticleListInteractorPayload(
            category: category
        ))
    }
    
    func initialSetup(){
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.backgroundColor = .clear
        searchBar.searchTextField.backgroundColor = .lightGray
        lblCategoriesTitle.text = category.capitalizingFirstLetter()
        imgCategoriesTitle.image = UIImage(named: category + ".jpg")
        
        navigationItem.titleView?.backgroundColor = .clear
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        navigationItem.titleView = searchBar
        setupTableView()
        self.loadingView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
    }
    
    func setupTableView(){
       self.tblArticlesList.delegate = self
       self.tblArticlesList.dataSource = self
       self.tblArticlesList.register(UINib(nibName: "ArticleListTVCell", bundle: nil), forCellReuseIdentifier: ArticleListTVCell.identifier)
    }
    
    func showLoading(){
        DispatchQueue.main.async {
            self.loadingView.isHidden = false
            self.loadingIndicator.startAnimating()
        }
    }
    
    func hideLoading(){
        DispatchQueue.main.async {
            self.loadingView.isHidden = true
            self.loadingIndicator.stopAnimating()
        }
    }

}

extension ArticleListVC : ArticleListProtocol {
    func isRetrievingData(isRetrieving: Bool) {
        if isRetrieving {
            self.showLoading()
        }else {
            self.hideLoading()
        }
    }
    
    func didGetData() {
        DispatchQueue.main.async {
            self.tblArticlesList.reloadData()
            self.hideLoading()
        }
    }
    
    
}

extension ArticleListVC : UISearchBarDelegate {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
        
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
           interactor.didLoad(payload: ArticleListInteractorPayload(
                category: category
            ))
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "articleSearchResultsVC") as! ArticleSearchResultsVC
        vc.txSearch = searchBar.text ?? ""
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

extension ArticleListVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.articleSources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
                                    withIdentifier: ArticleListTVCell.identifier,
                                    for: indexPath) as! ArticleListTVCell
        cell.configCell(source: interactor.articleSources[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "articleDetailVC") as! ArticleDetailVC
        vc.loadUrl = interactor.articleSources[indexPath.row].url
       self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      
    }
    
    
    
}
