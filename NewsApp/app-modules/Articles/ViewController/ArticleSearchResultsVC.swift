//
//  ArticleSearchResultsVC.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import UIKit

class ArticleSearchResultsVC: UIViewController {
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tblSearchResult: UITableView!
    
    var txSearch : String = ""
    var interactor = ArticleSearchResultsInteractor()
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Articles", bundle:nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        print(txSearch)
        self.showLoading() 
        interactor.delegate = self
        initialSetup()
        interactor.didLoad(keyword: txSearch)
    }
    
    
    func initialSetup(){
        setupTableView()
    }
    
    func setupTableView(){
       self.tblSearchResult.delegate = self
       self.tblSearchResult.dataSource = self
       self.tblSearchResult.register(UINib(nibName: "ArticleListTVCell", bundle: nil), forCellReuseIdentifier: ArticleListTVCell.identifier)
    }
    
    func showLoading(){
        self.loadingIndicator.startAnimating()
        self.loadingIndicator.isHidden = false
        self.tblSearchResult.isHidden = true
    }
    
    func hideLoading(){
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true
        self.tblSearchResult.isHidden = false
    }

}

extension ArticleSearchResultsVC : ArticleSearchResultProtocol {
    func didGetData() {
        DispatchQueue.main.async {
            self.hideLoading()
            self.tblSearchResult.reloadData()
        }
    }
    
    func didFailGetData() {
        self.showLoading()
    }
    
    
}

extension ArticleSearchResultsVC : UITableViewDataSource, UITableViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
          let height = scrollView.frame.size.height
          let contentYoffset = scrollView.contentOffset.y
          let distanceFromBottom = scrollView.contentSize.height - contentYoffset
          if distanceFromBottom < height {
              interactor.didLoad(keyword: txSearch)
          }
      }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(
                                           withIdentifier: ArticleListTVCell.identifier,
                                           for: indexPath) as! ArticleListTVCell
        cell.configCell(element: interactor.sources[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "articleDetailVC") as! ArticleDetailVC
        vc.loadUrl = interactor.sources[indexPath.row].url
        print(interactor.sources[indexPath.row].url)
        self.present(vc, animated: true, completion: nil)
    }
       
    
    
}
