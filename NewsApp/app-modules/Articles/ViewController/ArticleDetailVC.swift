//
//  ArticleDetailVC.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import UIKit
import WebKit

class ArticleDetailVC: UIViewController {

    var loadUrl = ""
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    func initialSetup(){
       let link = URL(string:loadUrl)!
       let request = URLRequest(url: link)
        webView.load(request)
    }
    

}
