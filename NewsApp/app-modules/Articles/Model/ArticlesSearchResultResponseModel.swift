//
//  ArticlesSearchResultResponseModel.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import Foundation

struct ArticlesSearchResultResponseModel: Codable {
    let totalResults: Int
    let articles: [ArticleElement]
}

struct ArticleElement: Codable {
    let author, title, articleDescription: String
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case author, title
        case articleDescription = "description"
        case url
    }
}
