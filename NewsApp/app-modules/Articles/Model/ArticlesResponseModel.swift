//
//  ArticlesResponseModel.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import Foundation

struct ArticlesResponseModel: Codable {
    let status: String
    let sources: [ArticleSource]
}

struct ArticleSource: Codable {
    let id, name, sourceDescription: String
    let url: String
    let language, country: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case sourceDescription = "description"
        case url, language, country
    }
}
