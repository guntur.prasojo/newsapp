//
//  DashboardInteractor.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import Foundation

protocol DashboardProtocol: class{
    func getCategories()
}

class DashboardInteractor{
    var delegate : DashboardProtocol?
    var categories : [String] = []
    
    func didLoad(){
        categories = FileManager.getPlist(withName: "NewsCategories") ?? []
        delegate?.getCategories()
    }
    
    func getSelectedCategories(_ index : Int) -> String{
        return categories[index]
    }
}
