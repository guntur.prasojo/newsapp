//
//  DashboardVC.swift
//  NewsApp
//
//  Created by Guntur Budi on 05/05/20.
//  Copyright © 2020 JATIS. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {
 
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    let searchBar = BaseSearchBar()
    
    var interactor = DashboardInteractor()
    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Articles", bundle:nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.delegate = self
        initialSetup()
        interactor.didLoad()
    }
    
    func initialSetup(){
        setupCollectionView()
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.searchTextField.backgroundColor = .lightGray
        navigationItem.titleView = searchBar
    }
    
    func setupCollectionView(){
        self.categoriesCollectionView.delegate = self
        self.categoriesCollectionView.dataSource = self
        self.categoriesCollectionView.register(UINib(nibName: "CategoriesCVCell", bundle: nil), forCellWithReuseIdentifier: CategoriesCollCell.identifier)
        if let layout = categoriesCollectionView?.collectionViewLayout as? BaseCollectionViewLayout {
          layout.delegate = self
        }
    }
    
}

extension DashboardVC : DashboardProtocol {
    
    func getCategories() {
        self.categoriesCollectionView.reloadData()
    }
}

extension DashboardVC : UICollectionViewDelegate, UICollectionViewDataSource ,
    BaseCollectionViewLayoutDelegate
{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
     func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 200
     }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interactor.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(
                                    withReuseIdentifier: CategoriesCollCell.identifier,
                                    for: indexPath) as! CategoriesCollCell
        cell.lblTitle.text = interactor.categories[indexPath.row].capitalizingFirstLetter()
        cell.imgBg.image = UIImage(named: interactor.categories[indexPath.row] + ".jpg")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "articlesStoryboard") as! ArticleListVC
        vc.modalPresentationStyle = .fullScreen
        vc.category = interactor.getSelectedCategories(indexPath.row)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension DashboardVC : UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let vc = storyBoard.instantiateViewController(withIdentifier: "articleSearchResultsVC") as! ArticleSearchResultsVC
        vc.txSearch = searchBar.text ?? ""
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
